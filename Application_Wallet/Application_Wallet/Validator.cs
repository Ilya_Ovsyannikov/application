﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application_Wallet
{
    class Validator
    {
        static private int CheckedId;
        
        static private int CheckedValue;
        
        public static bool Check(string value)
        {
            if (value == "")
            {
                return true;
            }
            
            else
            {
                if (int.TryParse(value, out CheckedValue))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }
        }

        public static bool Check(string id, string transactionType)
        {
            if (int.TryParse(id, out CheckedId))
            {
                switch (transactionType)
                {
                    case "income":
                        if (CheckedId < Profit.GetIncomeIdCounter() && CheckedId >= 0)
                        {
                            return true;
                        }

                        else
                        {
                            return false;
                        }
                    case "spending":
                        if (CheckedId < Profit.GetSpendingIdCounter() && CheckedId >= 0)
                        {
                            return true;
                        }

                        else
                        {
                            return false;
                        }
                    default:
                        return false;
                }
            }

            else
            {
                return false;
            }
        }

        public static bool Check(string id, string value, string transactionType)
        {
            if (Check(id, transactionType) && Check(value))
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}

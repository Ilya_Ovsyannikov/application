﻿using System;

namespace Application_Wallet
{
    class Program
    {
        static void Main(string[] args)
        {
            string id;
            
            string value;

            string comment;
            
            while (true)
            {
                Console.Write("\n1) Показать таблицу\n" +
                              "2) Добавить доход\n" +
                              "3) Добавить расход\n" +
                              "4) Удалить доход\n" +
                              "5) Удалить расход\n" +
                              "6) Изменить доход\n" +
                              "7) Изменить расход\n" +
                              "8) Вывести отчет в файл");
                Console.Write("\nВыберите действие(цифру): ");

                string choice = Console.ReadLine();

                switch (choice) 
                {
                    case "1":
                        Profit.PrintToConsole();
                        break;
                    case "2":
                        Console.Write("Введите сумму: ");
                        value = Console.ReadLine();

                        Console.Write("Добавьте комментарий(не обязательно): ");
                        comment = Console.ReadLine();

                        if (Validator.Check(value))
                        {
                            Profit.AddIncome(value, comment);
                        }

                        else
                        {
                            Console.WriteLine("|ОШИБКА ВВОДА|");
                        }

                        break;
                    case "3":
                        Console.Write("Введите сумму: ");
                        value = Console.ReadLine();

                        Console.Write("Добавьте комментарий(не обязательно): ");
                        comment = Console.ReadLine();

                        if (Validator.Check(value))
                        {
                            Profit.AddSpending(value, comment);
                        }

                        else
                        {
                            Console.WriteLine("|ОШИБКА ВВОДА|");
                        }

                        break;
                    case "4":
                        Console.Write("Введите id удаляемого дохода: ");
                        id = Console.ReadLine();

                        if (Validator.Check(id, "income"))
                        {
                            Profit.DeleteIncome(int.Parse(id));
                        }

                        else
                        {
                            Console.WriteLine("|ОШИБКА ВВОДА|");
                        }
                        
                        break;
                    case "5":
                        Console.Write("Введите id удаляемого расхода: ");
                        id = Console.ReadLine();

                        if (Validator.Check(id, "spending"))
                        {
                            Profit.DeleteSpending(int.Parse(id));
                        }

                        else
                        {
                            Console.WriteLine("|ОШИБКА ВВОДА|");
                        }

                        break;
                    case "6":
                        Console.Write("Введите id изменяемого дохода: ");
                        id = Console.ReadLine();

                        Console.Write("Введите новую сумму / оставьте пустым: ");
                        value = Console.ReadLine();

                        Console.Write("Введите новый комментарий / оставьте пустым, чтобы удалить: ");
                        comment = Console.ReadLine();

                        if (Validator.Check(id, value, "income"))
                        {
                            Profit.EditIncome(int.Parse(id), value, comment);
                        }

                        else
                        {
                            Console.WriteLine("|ОШИБКА ВВОДА|");
                        }

                        break;
                    case "7":
                        Console.Write("Введите id изменяемого расхода: ");
                        id = Console.ReadLine();

                        Console.Write("Введите новую сумму / оставьте пустым: ");
                        value = Console.ReadLine();

                        Console.Write("Введите новый комментарий / оставьте пустым, чтобы удалить: ");
                        comment = Console.ReadLine();

                        if (Validator.Check(id, value, "spending"))
                        {
                            Profit.EditSpending(int.Parse(id), value, comment);
                        }

                        else
                        {
                            Console.WriteLine("|ОШИБКА ВВОДА|");
                        }

                        break;
                    case "8":
                        Profit.PrintToFile();
                        break;
                    default:
                        Console.WriteLine("|ОШИБКА ВВОДА|");
                        break;
                }
            }
        }
    }
}

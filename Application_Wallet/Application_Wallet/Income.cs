﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application_Wallet
{
    class Income
    {
        private int id;

        private int value;

        private string comment;

        private bool isDeleted;
        
        public Income(int id, string value, string comment)
        {
            this.id = id;

            int.TryParse(value, out this.value);

            this.comment = comment;
        }

        public int GetId()
        {
            return id;
        }
        
        public int GetValue()
        {
            return value;
        }

        public string GetComment()
        {
            return comment;
        }
        
        public bool CheckDeletion()
        {
            return isDeleted;
        }
        
        public void ChangeValue(int newValue)
        {
            value = newValue;
        }

        public void ChangeComment(string newComment)
        {
            comment = newComment;
        }

        public void DeleteIncome()
        {
            isDeleted = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application_Wallet
{
    class Profit
    {
        private static StringBuilder sb = new StringBuilder();
        
        private static Income[] incomeList = new Income[5];

        private static Spending[] spendingList = new Spending[5];

        private static int profit = 0;

        private static int incomeIdCounter = 0;

        private static int spendingIdCounter = 0;

        public static void AddIncome(string value, string comment)
        {
            if (incomeIdCounter == incomeList.Length)
            {
                Array.Resize(ref incomeList, incomeList.Length * 2);
            }
            
            Income income = new Income(incomeIdCounter, value, comment);

            incomeList[incomeIdCounter] = income;

            profit += incomeList[incomeIdCounter].GetValue();

            incomeIdCounter++;
        }

        public static void AddSpending(string value, string comment)
        {
            if (spendingIdCounter == spendingList.Length)
            {
                Array.Resize(ref spendingList, spendingList.Length * 2);
            }

            Spending spending = new Spending(spendingIdCounter, value, comment);

            spendingList[spendingIdCounter] = spending;

            profit -= spendingList[spendingIdCounter].GetValue();

            spendingIdCounter++;
        }

        public static void DeleteIncome(int id)
        {
            profit -= incomeList[id].GetValue();

            incomeList[id].DeleteIncome();
        }

        public static void DeleteSpending(int id)
        {
            profit += spendingList[id].GetValue();

            spendingList[id].DeleteSpending();
        }

        public static void EditIncome(int id, string value, string comment)
        {
            if (value != "")
            {
                profit += int.Parse(value) - incomeList[id].GetValue();
                incomeList[id].ChangeValue(int.Parse(value));
            }

            incomeList[id].ChangeComment(comment);
        }

        public static void EditSpending(int id, string value, string comment)
        {
            if (value != "")
            {
                profit -= int.Parse(value) - spendingList[id].GetValue();
                spendingList[id].ChangeValue(int.Parse(value));
            }

            spendingList[id].ChangeComment(comment);
        }

        public static void PrintToConsole()
        {
            if (incomeList[0] != null)
            {
                Console.WriteLine("\n\t\tВАШ СПИСОК ДОХОДОВ\n");
                Console.WriteLine($"{"id", 5}" + $"{"Сумма", 11}" + "\t\tКомментарий\n");

                for (var i = 0; i < incomeList.Length; i++)
                {
                    if (incomeList[i] != null)
                    {
                        if (incomeList[i].CheckDeletion() == false)
                        {
                            Console.WriteLine($"{incomeList[i].GetId(),5}" + $"{incomeList[i].GetValue(),11}\t" + $"\t{incomeList[i].GetComment()}");
                        }
                    }
                }
            }

            else
            {
                Console.WriteLine("\n\t\tВАШ СПИСОК ДОХОДОВ ПУСТ\n");
            }

            if (spendingList[0] != null)
            {
                Console.WriteLine("\n\t\tВАШ СПИСОК РАСХОДОВ\n");
                Console.WriteLine($"{"id",5}" + $"{"Сумма",11}" + "\t\tКомментарий\n");

                for (var i = 0; i < spendingList.Length; i++)
                {
                    if (spendingList[i] != null)
                    {
                        if (spendingList[i].CheckDeletion() == false)
                        {
                            Console.WriteLine($"{spendingList[i].GetId(),5}" + $"{spendingList[i].GetValue(),11}\t" + $"\t{spendingList[i].GetComment()}");
                        }
                    }
                }
            }

            else
            {
                Console.WriteLine("\n\t\tВАШ СПИСОК РАСХОДОВ ПУСТ\n");
            }

            if (incomeList[0] != null || spendingList[0] != null)
            {
                Console.WriteLine($"\n\t\tПРИБЫЛЬ СОСТАВЛЯЕТ: {profit}");
            }
        }

        public static void PrintToFile()
        {
            if (incomeList[0] != null)
            {
                sb.AppendLine("\t\tВАШ СПИСОК ДОХОДОВ\n");
                sb.AppendLine($"{"id", 5}" + $"{"Сумма", 11}" + "\t\tКомментарий\n");

                for (var i = 0; i < incomeList.Length; i++)
                {
                    if (incomeList[i] != null)
                    {
                        if (incomeList[i].CheckDeletion() == false)
                        {
                            sb.AppendLine($"{incomeList[i].GetId(),5}" + $"{incomeList[i].GetValue(),11}\t" + $"\t{incomeList[i].GetComment()}");
                        }
                    }
                }
            }

            else
            {
                sb.AppendLine("\n\t\tВАШ СПИСОК ДОХОДОВ ПУСТ\n");
            }

            if (spendingList[0] != null)
            {
                sb.AppendLine("\n\t\tВАШ СПИСОК РАСХОДОВ\n");
                sb.AppendLine($"{"id",5}" + $"{"Сумма",11}" + "\t\tКомментарий\n");

                for (var i = 0; i < spendingList.Length; i++)
                {
                    if (spendingList[i] != null)
                    {
                        if (spendingList[i].CheckDeletion() == false)
                        {
                            sb.AppendLine($"{spendingList[i].GetId(),5}" + $"{spendingList[i].GetValue(),11}\t" + $"\t{spendingList[i].GetComment()}");
                        }
                    }
                }
            }

            else
            {
                sb.AppendLine("\n\t\tВАШ СПИСОК РАСХОДОВ ПУСТ\n");
            }

            if (incomeList[0] != null || spendingList[0] != null)
            {
                sb.AppendLine($"\n\t\tПРИБЫЛЬ СОСТАВЛЯЕТ: {profit}\n");
            }

            System.IO.File.WriteAllText($@"c:\users\{Environment.UserName}\desktop\report.txt", sb.ToString());
            Console.WriteLine("|ОТЧЕТ СОЗДАН НА РАБОЧЕМ СТОЛЕ|");

            sb.Clear();
        }

        public static int GetIncomeIdCounter()
        {
            return incomeIdCounter;
        }

        public static int GetSpendingIdCounter()
        {
            return spendingIdCounter;
        }
    }
}

